webpackJsonp([1],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/NewItems.vue":
/***/ (function(module, exports) {

throw new Error("Module build failed: SyntaxError: Unexpected token, expected , (72:16)\n\n\u001b[0m \u001b[90m 70 | \u001b[39m            \u001b[33mEvent\u001b[39m\u001b[33m.\u001b[39m$emit(\u001b[32m'Itemclicked'\u001b[39m\u001b[33m,\u001b[39m {\n \u001b[90m 71 | \u001b[39m                \u001b[32m\"Item\"\u001b[39m\u001b[33m:\u001b[39m item\n\u001b[31m\u001b[1m>\u001b[22m\u001b[39m\u001b[90m 72 | \u001b[39m                \u001b[32m\"Type\"\u001b[39m\u001b[33m:\u001b[39m \n \u001b[90m    | \u001b[39m                \u001b[31m\u001b[1m^\u001b[22m\u001b[39m\n \u001b[90m 73 | \u001b[39m            })\n \u001b[90m 74 | \u001b[39m        }\n \u001b[90m 75 | \u001b[39m    }\u001b[0m\n");

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d15cb37\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/NewItems.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "panel panel-default" }, [
    _c("div", { staticClass: "panel-body" }, [
      Object.keys(_vm.Artists).length != 0
        ? _c("div", { staticClass: "panel panel-default" }, [
            _c(
              "div",
              { staticClass: "panel-heading" },
              [
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.Movies.title) +
                    "\n                "
                ),
                _c(
                  "router-link",
                  {
                    staticClass: "pull-right",
                    attrs: { to: "/browsepage/movies" }
                  },
                  [_vm._v("More")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c(
                "div",
                { staticClass: "row row-horizon" },
                _vm._l(_vm.Movies.movies_result, function(movie) {
                  return _c(
                    "div",
                    {
                      key: movie.id,
                      staticClass: "col-sm-4",
                      on: {
                        click: function($event) {
                          _vm.Itemclicked(movie, "movies")
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "card" }, [
                        _c("img", {
                          staticClass: "card-img-top",
                          attrs: { src: movie.Poster, alt: movie.Title }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-block" }, [
                          _c("h4", { staticClass: "card-title" }, [
                            _vm._v(_vm._s(movie.Title))
                          ])
                        ])
                      ])
                    ]
                  )
                })
              )
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      Object.keys(_vm.Series).length != 0
        ? _c("div", { staticClass: "panel panel-default" }, [
            _c(
              "div",
              { staticClass: "panel-heading" },
              [
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.Series.title) +
                    "\n                "
                ),
                _c(
                  "router-link",
                  {
                    staticClass: "pull-right",
                    attrs: { to: "/browsepage/series" }
                  },
                  [_vm._v("More")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c(
                "div",
                { staticClass: "row row-horizon" },
                _vm._l(_vm.Series.series_result, function(serie) {
                  return _c(
                    "div",
                    {
                      key: serie.id,
                      staticClass: "col-sm-4",
                      on: {
                        click: function($event) {
                          _vm.Itemclicked(serie, "series")
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "card" }, [
                        _c("img", {
                          staticClass: "card-img-top",
                          attrs: { src: serie.Poster, alt: serie.Title }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-block" }, [
                          _c("h4", { staticClass: "card-title" }, [
                            _vm._v(_vm._s(serie.Title))
                          ])
                        ])
                      ])
                    ]
                  )
                })
              )
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7d15cb37", module.exports)
  }
}

/***/ }),

/***/ "./resources/assets/js/components/NewItems.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/NewItems.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d15cb37\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/NewItems.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/NewItems.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d15cb37", Component.options)
  } else {
    hotAPI.reload("data-v-7d15cb37", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});