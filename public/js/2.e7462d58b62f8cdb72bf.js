webpackJsonp([2],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/NewItems.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            Movies: [],
            Series: [],
            Artists: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/newitems').then(function (response) {
            for (var i = 0; i < response.data.length; i++) {
                response.data[i].type == "Movies" ? _this.Movies = response.data[i] : response.data[i].type == "Series" ? _this.Series = response.data[i] : _this.Artists = response.data[i];
            }
        });
    },

    methods: {
        Itemclicked: function Itemclicked(item) {
            Event.$emit('Itemclicked', item);
        }
    }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/component-normalizer.js":
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d15cb37\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/NewItems.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "panel panel-default" }, [
    _c("div", { staticClass: "panel-body" }, [
      Object.keys(_vm.Artists).length != 0
        ? _c("div", { staticClass: "panel panel-default" }, [
            _c(
              "div",
              { staticClass: "panel-heading" },
              [
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.Movies.title) +
                    "\n                "
                ),
                _c(
                  "router-link",
                  {
                    staticClass: "pull-right",
                    attrs: { to: "/browsepage/movies" }
                  },
                  [_vm._v("More")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c(
                "div",
                { staticClass: "row row-horizon" },
                _vm._l(_vm.Movies.movies_result, function(movie) {
                  return _c(
                    "div",
                    {
                      key: movie.id,
                      staticClass: "col-sm-4",
                      on: {
                        click: function($event) {
                          _vm.Itemclicked(movie)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "card" }, [
                        _c("img", {
                          staticClass: "card-img-top",
                          attrs: { src: movie.Poster, alt: movie.Title }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-block" }, [
                          _c("h4", { staticClass: "card-title" }, [
                            _vm._v(_vm._s(movie.Title))
                          ])
                        ])
                      ])
                    ]
                  )
                })
              )
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      Object.keys(_vm.Series).length != 0
        ? _c("div", { staticClass: "panel panel-default" }, [
            _c(
              "div",
              { staticClass: "panel-heading" },
              [
                _vm._v(
                  "\n                " +
                    _vm._s(_vm.Series.title) +
                    "\n                "
                ),
                _c(
                  "router-link",
                  {
                    staticClass: "pull-right",
                    attrs: { to: "/browsepage/series" }
                  },
                  [_vm._v("More")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "panel-body" }, [
              _c(
                "div",
                { staticClass: "row row-horizon" },
                _vm._l(_vm.Series.series_result, function(serie) {
                  return _c(
                    "div",
                    {
                      key: serie.id,
                      staticClass: "col-sm-4",
                      on: {
                        click: function($event) {
                          _vm.Itemclicked(serie)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "card" }, [
                        _c("img", {
                          staticClass: "card-img-top",
                          attrs: { src: serie.Poster, alt: serie.Title }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-block" }, [
                          _c("h4", { staticClass: "card-title" }, [
                            _vm._v(_vm._s(serie.Title))
                          ])
                        ])
                      ])
                    ]
                  )
                })
              )
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7d15cb37", module.exports)
  }
}

/***/ }),

/***/ "./resources/assets/js/components/NewItems.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/components/NewItems.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-7d15cb37\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/components/NewItems.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/NewItems.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d15cb37", Component.options)
  } else {
    hotAPI.reload("data-v-7d15cb37", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});