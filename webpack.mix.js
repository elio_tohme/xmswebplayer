// let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

const { mix } = require('laravel-mix');
mix.setPublicPath('public/');

mix.webpackConfig({
    output: {
      chunkFilename: 'js/[name].[chunkhash].js',
         publicPath: '/'
     },
    module: {
        rules: [{
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: ['babel-preset-env'],
              plugins:['babel-plugin-dynamic-import-webpack']
            }
        },

  ]
    }
});


mix.js('resources/assets/js/app.js', 'public/js')
.sass('resources/assets/sass/app.scss', 'public/css')
.version();


/**
 * adding shakaplayer to the mix ;) 
 */
mix.scripts("resources/assets/js/shaka-player.compiled.js", "public/js/play.js");

mix.styles('resources/assets/sass/theme.css', 'public/theme.css');