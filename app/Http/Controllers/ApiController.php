<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function getNewitems () {
    	$client = new Client();
    	return $client->request('GET', 'http://8o8o8o.net:9090/getnewitems.php')->getBody();
    }

    public function getMovies ($genre) {
    	$client = new Client();
    	
    	$result = $client->request('POST', 'http://8o8o8o.net:9090/getmoviesmpd.php', [
    		'json' => [['genre' => $genre]]
    	])->getBody();

    	return json_decode($result);
    }

    public function getSeries ($genre) {
		$client = new Client();
    	
    	$result = $client->request('POST', 'http://8o8o8o.net:9090/getseries.php', [
    		'json' => [['genre' => $genre]]
    	])->getBody();
    	return json_decode($result);
    }

    public function getGenres ($type)
    {
        $client = new Client();
        
        $result = $client->request('POST', 'http://8o8o8o.net:9090/getgenres.php', [
            'json' => [['Type' => ucfirst($type)]]
        ])->getBody();
        $result = json_decode($result, true);
        array_unshift($result, ["genre_id" => 9999, "genre_name" => "All"]);

        return json_encode($result);   
    }

    public function getSeasons ($series_id)
    {
        $client = new Client();
        
        $result = $client->request('POST', 'http://8o8o8o.net:9090/getseasons.php', [
            'json' => [['id' => $series_id]]
        ])->getBody();
        return json_decode($result);
    }

    public function getEpisodes ($series_id, $season)
    {
        $client = new Client();
        
        $result = $client->request('POST', 'http://8o8o8o.net:9090/getepisodes.php', [
            'json' => [['serieID' => $series_id, 'season' => $season]]
        ])->getBody();
        return json_decode($result);
    }
}