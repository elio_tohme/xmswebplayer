@extends('layouts.app')

@section('content')
<div >
	@guest
	<div class="container">
		<div class="row">
			<div class="col-md-12">
		  		<router-view></router-view>
		  	</div>	
		</div>
	</div>	
	@else
		<div class="col-md-5 col-md-push-7">
			<xmsplayer></xmsplayer>
		</div>
	  	<div class="col-md-7 col-md-pull-5 screen-height">
	  		<router-view></router-view>
	  	</div>
	@endguest
</div>	

@endsection
