
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueRouter from 'vue-router'
import 'muxjs'
import router from './router'
import VueLazyload from 'vue-lazyload'
import VModal from 'vue-js-modal'

// set event bus for component comunication 
window.Event = new Vue();

// load VueRouter
Vue.use(VueRouter)

// Load Vuelazyload for lazy loading images
Vue.use(VueLazyload)

// load Vmodal
Vue.use(VModal)

// always load XmsPlayer
import XmsPlayer from './components/XmsPlayer.vue'

// init Vue instance
new Vue({
  components : {
    "xmsplayer": XmsPlayer
  },
  router,
}).$mount('#app')
