import VueRouter    from 'vue-router'

// lazy load components
const genres = () => import('./components/Genres.vue')
const newitems = () => import('./components/NewItems.vue')
const browse = () => import('./components/Browse.vue')

export default new VueRouter({
    mode: 'history',
    base: __dirname,
	routes: [
		{ path: '/genrespage', component: genres},
		{ path: '/newitemspage', component: newitems},
		{ path: '/browsepage/:Type', component: browse, props: true}
	]
});
