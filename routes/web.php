<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('newitemspage');
});

Route::get('{vue_capture?}', function () {
        return response()->view('home');
    })->where('vue_capture', '\w*page\b');

Route::get('{vue_capture?}/{secondparam}', function () {
        return response()->view('home');
    })->where('vue_capture', '\w*page\b');

Route::get('newitems', 'ApiController@getNewitems');
Route::get('/movies/{genre}', 'ApiController@getMovies');
Route::get('/series/{genre}', 'ApiController@getSeries');
Route::get('/genres/{type}', 'ApiController@getGenres');
Route::get('/seasons/{series_id}' , 'ApiController@getSeasons');
Route::get('/episodes/{series_id}/{season}' , 'ApiController@getEpisodes');

Auth::routes();

Route::get('/home', function () {
	return redirect('/newitemspage');
});
